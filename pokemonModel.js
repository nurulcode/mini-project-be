const mongoose = require('mongoose');

// Define the schema for Pokemon
const pokemonSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    version: {
        type: Number,
        required: true
    },
});

const Pokemon = mongoose.model('Pokemon', pokemonSchema);

module.exports = Pokemon;