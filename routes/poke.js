var express = require("express");
var router = express.Router();
const axios = require("axios");
const { ifError } = require("assert");
const Pokemon = require("../pokemonModel");

const itemsPerPage = 10;

/* GET users listing. */
router.get("/pokemons", async (req, res) => {
    try {
        const { page = 1 } = req.query;
        const offset = (page - 1) * itemsPerPage;
        const response = await axios.get(
            `${process.env.URL}pokemon?limit=${itemsPerPage}&offset=${offset}`
        );
        const pokemonList = response.data.results;

        const formattedList = pokemonList.map((pokemon, index) => ({
            id: index + 1 + offset,
            name: pokemon.name,
            url: pokemon.url,
        }));

        res.json({
            page: parseInt(page),
            itemsPerPage,
            pokemon: formattedList,
        });
    } catch (error) {
        res.status(500).json({ message: "Error fetching Pokemon list" });
    }
});

router.get("/pokemon/:idorname", async (req, res) => {
    const { idorname } = req.params;
    try {
        const response = await axios.get(
            `${process.env.URL}pokemon/${idorname}`
        );
        const pokemonData = response.data;

        res.json({ pageTitle: "Pokemen Detail", pokemen: pokemonData });
    } catch (error) {
        res.status(404).json({ message: "Error get Pokemon detail" });
    }
});

function randomNumber() {
    return Math.floor(Math.random() * (100 - 10 + 1)) + 10;
}

function isPrime() {
    // filter prime or no
    const number = randomNumber();
    if (number <= 1) {
        return false;
    }
    for (let i = 2; i <= Math.sqrt(number); i++) {
        if (number % i === 0) {
            return false;
        }
    }
    return true;
}

function generateFibonacciSequence(limit) {
    let fibonacciSequence = [0, 1];
    let nextNumber = 1;

    while (nextNumber <= limit) {
        nextNumber =
            fibonacciSequence[fibonacciSequence.length - 1] +
            fibonacciSequence[fibonacciSequence.length - 2];
        if (nextNumber <= limit) {
            fibonacciSequence.push(nextNumber);
        }
    }

    return fibonacciSequence;
}

router.get("/persentase", (req, res) => {
    const persentase = randomNumber();
    let status = false;
    if (persentase >= 50) {
        status = true;
    }

    return res.json({
        message: "is persentase",
        data: {
            persentase,
            status,
        },
    });
});

router.get("/prime", (req, res) => {
    const persentase = randomNumber();
    let status = false;
    if (persentase >= 50) {
        status = true;
    }
    const prima = isPrime(persentase);

    return res.json({
        message: "is prime",
        data: {
            persentase,
            prima,
        },
    });
});

router.get("/sequence", (req, res) => {
    const { version } = req.body;
    let limit = version ? 100 * version : version;

    const number = generateFibonacciSequence(limit);
    const sequence = number[version];

    return res.json({
        message: "is sequence",
        data: {
            sequence,
        },
    });
});

router.post("/save-pokemon", async (req, res) => {
    const foundPokemon = await Pokemon.findOne({
        name: `${req.body.name}`,
    });

    const persentase = randomNumber();
    let status = false;
    if (persentase >= 50) {
        status = true;
    }

    if (!status) {
        return res.status(200).json({
            message: "error",
            data: {
                status: "no",
                persentase
            },
        });
    } else {
        const newPokemon = new Pokemon({
            name: `${req.body.name}-0`,
            content: req.body.content,
            version: 0,
        });

        newPokemon
            .save()
            .then((savedPokemon) => {
                return res.json({
                    message: "success",
                    data: {
                        status: "redirect",
                        persentase
                    },
                });
            })
            .catch((err) => {
                return res.json({
                    message: "error",
                    data: {
                        status: "no",
                        persentase
                    },
                });
            });
    }
});

router.post("/update-pokemon", async (req, res) => {
    const foundPokemon = await Pokemon.findOne({
        name: `${req.body.name}`,
    });

    if (foundPokemon) {
        let limit = foundPokemon.version ? 100 * foundPokemon.version : 100;

        const number = generateFibonacciSequence(limit);
        const sequence = number[foundPokemon.version + 1];
        let newName = `${foundPokemon.name.split("-")[0]}-${sequence}`;

        foundPokemon.name = newName;
        foundPokemon.version = foundPokemon.version + 1;
        const updatePokemon = await foundPokemon.save();
        return res.json({
            status: "redirect",
            data: updatePokemon,
        });
    } else {
        return res.status(404).json({
            status: false,
            message: "error",
        });
    }
});

router.get("/list-pokemons", async (req, res) => {
    const keyword = req.query.keyword
        ? {
              name: {
                  $regex: req.query.keyword,
                  $options: "i",
              },
          }
        : {};

    const pokemon = await Pokemon.find({ ...keyword });

    return res.status(201).json({
        message: "is pokemons",
        data: pokemon,
    });
});

router.delete("/list-pokemon/:id/rilis", async (req, res) => {
    const foundPokemon = await Pokemon.findOne({
        name: `${req.params.id}`,
    });

    const persentase = randomNumber();
    let status = false;
    if (persentase >= 50) {
        status = true;
    }
    const prima = isPrime(persentase);
    if (foundPokemon && prima) {
        await Pokemon.deleteOne({ _id: foundPokemon._id });
        return res.json({
            message: "success",
            data: {
                status: "redirect",
                persentase,
                prima,
            },
        });
    } else {
        return res.json({
            message: "error",
            data: {
                status: "no",
                persentase,
                prima,
            },
        });
    }
});

module.exports = router;
